<!-- start: Header -->
<div class="navbar">
	<div class="navbar-inner">
		<div class="container-fluid">
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<a class="brand" href="<?=base_url()?>"><span style="font-weight: bolder;">Sampark 2.0 </span></a>
			
			<!-- start: Header Menu -->
			<div class="nav-no-collapse header-nav">
				<ul class="nav pull-right">

					<!-- start: User Dropdown -->
					<li class="dropdown" style="">
						<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
							<i class="halflings-icon white user"></i> <span style="font-weight: bolder;text-transform: capitalize;"><?=$_SESSION['name']?></span>
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
							<li class="dropdown-menu-title">
								<span>Account Settings</span>
							</li>
							<li ><a href="#"><i class="halflings-icon user"></i> <span style="text-transform: capitalize;"><?=$_SESSION['type']=='developers'?'users':$_SESSION['type']?></span></a></li>
							<li><a href="<?=base_url()?>auth/logout"><i class="halflings-icon off"></i> Logout</a></li>
						</ul>
					</li>
					<!-- end: User Dropdown -->
				</ul>
			</div>
			<!-- end: Header Menu -->

		</div>
	</div>
</div>
<!-- start: Header -->
<div class="container-fluid-full">
	<div class="row-fluid">

		<!-- start: Main Menu -->
		<div id="sidebar-left" class="span2">
			<div class="nav-collapse sidebar-nav">
				<ul class="nav nav-tabs nav-stacked main-menu">
					<li><a href="<?=base_url()?>dashboard"><i class="icon-dashboard"></i><span class="hidden-tablet"> Dashboard</span></a></li>
					<?php if($_SESSION['type']== 'users' || $_SESSION['type']== 'developers' ){?>
					<li><a href="<?=base_url()?>action/view_ticket"><i class="icon-bar-chart"></i><span class="hidden-tablet"> Tickets</span></a></li>
					<li><a href="<?=base_url()?>action/change_password"><i class="icon-edit"></i><span class="hidden-tablet"> Change Password</span></a></li>
					<?php }?>
					<?php if($_SESSION['type']== 'admin' || $_SESSION['type']== 'superadmin' || $_SESSION['type']== 'developers' ):?>
					<li><a href="<?=base_url()?>pending_ticket"><i class="icon-eye-open"></i><span class="hidden-tablet"> Pending</span></a></li>
					<li><a href="<?=base_url()?>compelted"><i class="icon-star"></i><span class="hidden-tablet"> Completed</span></a></li>

						<?php if($_SESSION['type']== 'superadmin' || $_SESSION['type']== 'developers' ):?>
					<li><a href="<?=base_url()?>users"><i class="icon-lock"></i><span class="hidden-tablet"> Users</span></a></li>
					
					<?php endif;?>

					<?php endif;?>
					
	</ul>
			</div>
		</div>
		<!-- end: Main Menu -->

		<noscript>
			<div class="alert alert-block span10">
				<h4 class="alert-heading">Warning!</h4>
				<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
			</div>
		</noscript>

