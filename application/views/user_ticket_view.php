<!-- start: Content -->
<div id="content" class="span10">


	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="<?=base_url()?>">Home</a>
			<i class="icon-angle-right"></i>
		</li>
		<li><a href="#">Issue</a></li>
	</ul>
	<div class="row-fluid sortable">
		<div class="box span12">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon white user"></i><span class="break"></span>Issue List</h2>
				<div class="box-icon">
					<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped table-bordered bootstrap-datatable datatable">
					<thead>
					<tr>
						<th>#</th>
						<th>Issueid</th>
						<th>Alloted Date</th>
						<th>Resolving Date</th>
						<th>Username</th>
						<th>Issue</th>
						<th>Floor</th>
						<th>Workstation</th>
						<th>Status</th>
						<th>Assigned</th>
						<th>Processing Comment</th>

					</tr>
					</thead>
					<tbody>
					<?php $i=1; foreach ($item as $idata){?>
					<tr>
						<td><?=$i++?></td>
						<td>Softwill<?=$idata['id']?></td>

						<?php 
						$old_date = date($idata['created']);              // returns Saturday, January 30 10 02:06:34
						$old_date_timestamp = strtotime($old_date);
						$new_date = date('F , d y h:i:s', $old_date_timestamp);?>
						<td><?=$new_date?></td>
						<?php 
						$old_date1 = date($idata['modified']);              // returns Saturday, January 30 10 02:06:34
						$old_date_timestamp1 = strtotime($old_date1);
						$new_date1 = date('F , d y h:i:s', $old_date_timestamp1);?>
						<td><?=$new_date1?></td>


						<td><?=$idata['username']?></td>
						<td><?=$idata['issue']?></td>
						<td><?=$idata['floor']?></td>
						<td><?=$idata['workstation']?></td>
						<?php if($idata['status']=="pending"){?>
						<td><span class="label label-important"><?=$idata['status']?></span>	
						</td>
					<?php }elseif($idata['status']=="processing"){?>
						<td><span class="label label-warning"><?=$idata['status']?></span>	
						</td>
						<?php }else{?>
						<td><span class="label label-success"><?=$idata['status']?></span>	
						</td><?php }?>
						<?php if($idata['doer']==null){
							echo "<td>Not Assigned</td>";
						}else{?>
							<td><?=$idata['doer']?></td>
						<?php }?>
						<td><?=$idata['rescomment']?></td>
					</tr>
					<?php };?>
					</tbody>
				</table>
			</div>
		</div><!--/span-->

	</div><!--/row-->


</div><!--/.fluid-container-->

<!-- end: Content -->
</div><!--/#content.span10-->
</div><!--/fluid-row-->


<div class="clearfix"></div>
	