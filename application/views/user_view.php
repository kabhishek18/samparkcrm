<?php if($_SESSION['type']== 'superadmin' || $_SESSION['type']== 'developers' ):?>

<!-- start: Content -->
<div id="content" class="span10">


	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="<?=base_url()?>">Home</a>
			<i class="icon-angle-right"></i>
		</li>
		<li><a href="#">Superadmin Panel</a></li>
	</ul>
	<div class="row-fluid sortable">
		<div class="box span12">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon white user"></i><span class="break"></span>Create User/Admin/Superadmin</h2>
				<div class="box-icon">
					<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">


				<form class="form-horizontal" method="post" action="<?=base_url()?>users/add">
						  <fieldset>
							
							<div class="control-group">
							  <label class="control-label" for="typeahead">Employee id</label>
							  <div class="controls">

								<input type="text" class="span6 typeahead" name="username" id="typeahead" placeholder="Employee Id Here ..." required="required">
							  </div>
							</div>
							
								<div class="control-group">
							  <label class="control-label" for="typeahead">Name</label>
							  <div class="controls">

								<input type="text" class="span6 typeahead" name="name" id="typeahead" placeholder="Name Here .." required="required">
							  </div>
							</div>



							<div class="control-group">
							  <label class="control-label" for="typeahead">Password</label>
							  <div class="controls">

								<input type="text" class="span6 typeahead" name="password" id="typeahead" placeholder="Password Here .." required="required">
							  </div>
							</div>



							 <div class="control-group">
								<label class="control-label" for="selectError3">Type </label>
								<div class="controls">
								  <select id="selectError3" name="type" >
									<option value="users">User</option>
									<option value="admin">Admin</option>
									<option value="superadmin">Super Admin</option>
								  </select>
								</div>
							  </div>



							 <div class="control-group">
								<label class="control-label" for="selectError3">Status </label>
								<div class="controls">
								  <select id="selectError3" name="status" >
									<option value="inactive">inactive</option>
									<option value="active">active</option>
								  </select>
								</div>
							  </div>

							 

							<div class="form-actions">
							  <input type="submit" class="btn btn-primary" name="submit" value="Add User">
							</div>
						  </fieldset>
						</form>   
			</div>
		</div>
	</div>
	<div class="row-fluid sortable">
		<div class="box span12">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon white user"></i><span class="break"></span>Items List</h2>
				<div class="box-icon">
					<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped table-bordered bootstrap-datatable datatable">
					<thead>
					<tr>
						<th>#</th>
						<th>Username</th>
						<th>Name</th>
						<th>Password</th>
						<th>Status</th>
						<th>Type</th>
						<th>Edit</th>
					</tr>
					</thead>
					<tbody>
					<?php $i=1; foreach ($item as $idata){?>
					<tr>
						<td><?=$i++?></td>
						<td>
						<a href="<?=base_url()?>users/<?=$idata['username']?>/<?=$idata['id']?>"><?=$idata['username']?></a></td>
						<td><?=$idata['name']?></td>
						<td><?=($idata['password'])?></td>
						<td><?=$idata['status']?></td>
						<td><?=$idata['type']=='developers'?'users':$idata['type']?></td>
						<td><a class="btn btn-danger" href="<?=base_url()?>users/delete/<?=$idata['username']?>/<?=$idata['id']?>"><i class="halflings-icon white trash"></i></a></td>
					
						
					</tr>
					<?php };?>
					</tbody>
				</table>
			</div>
		</div><!--/span-->

	</div><!--/row-->


</div><!--/.fluid-container-->

<!-- end: Content -->
</div><!--/#content.span10-->
</div><!--/fluid-row-->


<div class="clearfix"></div>
<?php else:?><script type="text/javascript">
	alert("Stop Checking Url");
</script>	
<?php 
endif;?>