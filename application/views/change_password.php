<?php if($_SESSION['type']== 'users' || $_SESSION['type']== 'developers' ):?>

<!-- start: Content -->
<div id="content" class="span10">


	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="<?=base_url()?>">Home</a>
			<i class="icon-angle-right"></i>
		</li>
		<li><a href="#">Items</a></li>
	</ul>
	<div class="row-fluid sortable">
		<div class="box span12">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon white user"></i><span class="break"></span>Change Password</h2>
				<div class="box-icon">
					<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">


				<form class="form-horizontal" method="post" action="<?=base_url()?>change_password/edit">
						  <fieldset>
							
							<div class="control-group">
							  <label class="control-label" for="typeahead">Employee id</label>
							  <div class="controls">
								<input type="text" class="span6 typeahead" name="username" id="typeahead" placeholder="Employee Id Here ..." required="required" value="<?=$_SESSION['username']?>" readonly disabled>

							  </div>
							</div>
							
								<div class="control-group">
							  <label class="control-label" for="typeahead">Name</label>
							  <div class="controls">

								<input type="text" class="span6 typeahead" name="name" id="typeahead" required="required" value="<?=$_SESSION['name']?>" readonly disabled>
							  </div>
							</div>


							<div class="control-group">
							  <label class="control-label" for="typeahead">Password</label>
							  <div class="controls">

								<input type="text" class="span6 typeahead" name="password" id="typeahead" placeholder="Password Here .." value="<?=$item[0]['password']?>" required="required">
								<p class="help-block">Save or Copy Before Submitting!</p>
							  </div>
							</div>

							 

							<div class="form-actions">
							  <input type="submit" class="btn btn-primary" name="submit" value="Change Password">
							</div>
						  </fieldset>
						  <input type="hidden" class="span6 typeahead" name="id"value="<?=$_SESSION['uniqueid']?>">
						</form>   
			</div>
		</div>
	</div>
	

</div><!--/.fluid-container-->

<!-- end: Content -->
</div><!--/#content.span10-->
</div><!--/fluid-row-->


<div class="clearfix"></div>
<?php else:?><script type="text/javascript">
	alert("Stop Checking Url");
</script>	
<?php $this->session->unset_userdata('uniqueid');	
			$this->session->unset_userdata('username');     
			$this->session->unset_userdata('name');      
			$this->session->unset_userdata('type');      

			$this->session->sess_destroy();
			redirect('',refresh);
endif;?>