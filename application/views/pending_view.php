<!-- start: Content -->
<div id="content" class="span10">


	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="<?=base_url()?>">Home</a>
			<i class="icon-angle-right"></i>
		</li>
		<li><a href="#">Pending Issue</a></li>
	</ul>

	<div class="row-fluid sortable">
		<div class="box span12">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon white user"></i><span class="break"></span>Items List</h2>
				<div class="box-icon">
					<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped table-bordered bootstrap-datatable datatable">
					<thead>
					<tr>
						<th>#</th>
						<th>Issueid</th>
						<th>Alloted Date</th>
						<th>Username</th>
						<th>Issue</th>
						<th>Ucomment</th>
						<th>Floor</th>
						<th>Workstation</th>
						<th>ResComment</th>
						<th>Status</th>
						<th>Edit</th>
					</tr>
					</thead>
					<tbody>
					<?php $i=1; foreach ($item as $idata){?>
					<tr>
						<form method="post" action="<?=base_url()?>travis/done" onsubmit="return confirm('Are you sure <?=$_SESSION['username']?>!   Issue is being Updated ?');"> 
						<td><?=$i++?></td>
						<td>Softwill<?=$idata['id']?></td>

						<?php 
						$old_date = date($idata['created']);         
						$old_date_timestamp = strtotime($old_date);
						$new_date = date('F , d y h:i:s', $old_date_timestamp);?>
						<td><?=$new_date?></td>
						<td><?=$idata['username']?></td>
						<td><?=$idata['issue']?></td>
						<td><?=$idata['comment']?></td>
						<td><?=$idata['floor']?></td>
						<td><?=$idata['workstation']?></td>
						
							
						<td><textarea name="rescomment"><?=$idata['rescomment']?></textarea> </td>
						<td>  <select id="selectError1" name="status" style="text-transform: capitalize;">
								  	  <optgroup label="Selected">
								  	<option value="<?=$idata['status']?>" selected ><?=$idata['status']?>
								  		</option>
								  	  </optgroup>
								  	 <optgroup label="Non Selected"> 
									<option value="done">done</option>
									<option value="processing">Processing</option>
									</optgroup>
								  </select>
						</td>
					
							<input type="hidden" name="doer" value="<?=$_SESSION['username']?>">
							<input type="hidden" name="id" value="<?=$idata['id']?>">
						<td><input type="submit" class= "btn btn-warning"  name="submit" value="Click Me"></td>
						</form>
						
					</tr>
					<?php };?>
					</tbody>
				</table>
			</div>
		</div><!--/span-->

	</div><!--/row-->


</div><!--/.fluid-container-->

<!-- end: Content -->
</div><!--/#content.span10-->
</div><!--/fluid-row-->


<div class="clearfix"></div>
	