<?php if($_SESSION['type']== 'superadmin' || $_SESSION['type']== 'developers' ):?>

<!-- start: Content -->
<div id="content" class="span10">


	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="<?=base_url()?>">Home</a>
			<i class="icon-angle-right"></i>
		</li>
		<li><a href="#">User</a></li>
	</ul>
	<div class="row-fluid sortable">
		<div class="box span12">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon white user"></i><span class="break"></span>Edit</h2>
				<div class="box-icon">
					<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">


				<form class="form-horizontal" method="post" action="<?=base_url()?>users/update">
						  <fieldset>
							
							<div class="control-group">
							  <label class="control-label" for="typeahead">Employee id</label>
							  <div class="controls">
								<input type="text" class="span6 typeahead" name="username" id="typeahead" placeholder="Employee Id Here ..." required="required" value="<?=$item[0]['username']?>">

							  </div>
							</div>
							
								<div class="control-group">
							  <label class="control-label" for="typeahead">Name</label>
							  <div class="controls">

								<input type="text" class="span6 typeahead" name="name" id="typeahead" required="required" value="<?=$item[0]['name']?>">
							  </div>
							</div>


							<div class="control-group">
							  <label class="control-label" for="typeahead">Password</label>
							  <div class="controls">

								<input type="text" class="span6 typeahead" name="password" id="typeahead" placeholder="Password Here .." value="<?=$item[0]['password']?>" required="required">
							  </div>
							</div>



							 <div class="control-group">
								<label class="control-label" for="selectError3">Type </label>
								<div class="controls">
								  <select id="selectError3" name="type" style="text-transform: capitalize;">
								  	  <optgroup label="Selected">
								  	<option value="<?=$item[0]['type']?>" selected ><?=$item[0]['type']=='developers'?'user':$item[0]['type']?>
								  		</option>
								  	  </optgroup>
								  	 <optgroup label="Non Selected"> 
									<option value="users">User</option>
									<option value="admin">Admin</option>
									<option value="superadmin">Super Admin</option>
									<?php if($_SESSION['type']=='developers'):?>
									<option value="developers">Developers</option>
									<?php endif;?>
									</optgroup>
								  </select>
								</div>
							  </div>



							 <div class="control-group">
								<label class="control-label" for="selectError3">Status </label>
								<div class="controls">
								  <select id="selectError3" name="status" style="text-transform: capitalize;">
								  		  <optgroup label="Selected">
								  		<option value="<?=$item[0]['status']?>" selected >
								  		<?=$item[0]['status']?></option>
								  		  </optgroup>
									<optgroup label="Non Selected">
									<option value="inactive">inactive</option>
									<option value="active">active</option>
									</optgroup>
								  </select>
								</div>
							  </div>

							 

							<div class="form-actions">
							  <input type="submit" class="btn btn-primary" name="submit" value="Edit User">
							</div>
						  </fieldset>
						  <input type="hidden" class="span6 typeahead" name="id"value="<?=$item[0]['id']?>">
						</form>   
			</div>
		</div>
	</div>
	

</div><!--/.fluid-container-->

<!-- end: Content -->
</div><!--/#content.span10-->
</div><!--/fluid-row-->


<div class="clearfix"></div>
<?php else:?><script type="text/javascript">
	alert("Stop Checking Url");
</script>	
<?php 
endif;?>