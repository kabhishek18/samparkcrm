<!-- start: Content -->
<div id="content" class="span10">


	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="<?=base_url()?>">Home</a>
			<i class="icon-angle-right"></i>
		</li>
		<li><a href="#">Dashboard</a></li>
	</ul>



	<div class="row-fluid">
		
	<?php if($_SESSION['type']== 'admin' || $_SESSION['type']== 'superadmin' ):?>
			<a class="quick-button metro yellow span2">
			<i class="icon-group"></i>
			<p>Users</p>
			<span class="badge"><?=$user?></span>
		</a>
		<a class="quick-button metro blue span2">
			<i class="icon-shopping-cart"></i>
			<p>Admin</p>
			<span class="badge"><?=$admin?></span>
		</a>
		<a class="quick-button metro green span2">
			<i class="icon-barcode"></i>
			<p>Superadmin</p>
			<span class="badge"><?=$superadmin?></span>
		</a>
	<?php elseif($_SESSION['type']== 'users'):?>
			<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon white edit"></i><span class="break"></span>Ticket Generater</h2>
						<div class="box-icon">
							<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<form class="form-horizontal" method="post" action="<?=base_url()?>ticket">
						  <fieldset>
							
							<div class="control-group">
							  <label class="control-label" for="typeahead">Employee id</label>
							  <div class="controls">

								<input type="text" class="span6 typeahead" id="typeahead" value="<?=$_SESSION['username']?>" readonly disabled>
							  </div>
							</div>
							
							 <div class="control-group">
								<label class="control-label" for="selectError3">WorkStation</label>
								<div class="controls">
									<select id="selectError3" name="workstation" required="required">
									<?php 
										for ($i=1; $i <72 ; $i++) { 
											echo "<option value=".$i.">".$i."</option>";			
										}
										echo "</select>";
									?>
								</div>
							  </div>

							 
							 <div class="control-group">
								<label class="control-label" for="selectError3">Floor </label>
								<div class="controls">
								  <select id="selectError3" name="floor" >
									<option value="2nd Floor">2nd Floor</option>
									<option value="3rd Floor">3rd Floor</option>
									<option value="4th Floor">4th Floor</option>
								  </select>
								</div>
							  </div>

							<div class="control-group">
								<label class="control-label" for="selectError2">Issue Select</label>
								<div class="controls">
									<select data-placeholder="Types Of Issue" name="issue" id="selectError2" data-rel="chosen" required="required">
										<option value=""></option>
										<optgroup label="Desktop">
										  <option>Keyboard</option>
										  <option>Mouse</option>
										  <option>Headphone</option>
										  <option>Monitor</option>
										  <option>CPU</option>
										  <option>Power Suppply</option>
										  <option>Software</option>
										  <option>Hang</option>
										</optgroup>
										<optgroup label="Server & Network">
										  <option>Password Reset</option>
										  <option>Log On </option>
										  <option>Five9</option>
										  <option>8*8</option>
										  <option>AVOXI </option>
										  <option>Voice Issue </option>
										  <option>Website Issue </option>
										  <option>Website Open Request </option>
										  <option>Convox</option>
										  <option>Mail Password Reset</option>
										  <option>LMI Sesion Required</option>
										  <option>Call Recording Required</option>
										  <option>Shared Drive </option>
										  <option>Access Point</option>
										  <option>Convox</option>
										</optgroup>
										<optgroup label="Others">
										  <option>Others</option>
										</optgroup>
								  </select>
								</div>
							  </div>


							<div class="control-group hidden-phone">
							  <label class="control-label" for="textarea2">Comment Here</label>
							  <div class="controls">
								<textarea class="cleditor" id="textarea2" rows="3" name="comment"></textarea>
							  </div>
							</div>
							<div class="form-actions">
							  <input type="submit" class="btn btn-primary" name="submit" value="Generate">
							</div>
						  </fieldset>
						  <input type="hidden" name="username" value="<?=$_SESSION['username']?>">
						</form>   

					</div>
				</div><!--/span-->

			</div><!--/row-->
	<?php elseif($_SESSION['type']== 'developers'):?>
		

			<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon white edit"></i><span class="break"></span>Ticket Generater</h2>
						<div class="box-icon">
							<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<div class="box-content">
							<a class="quick-button metro yellow span2" >
							<i class="icon-group"></i>
							<p>Users</p>
							<span class="badge"><?=$user?></span>
							</a>
							<a class="quick-button metro blue span2" >
							<i class="icon-shopping-cart"></i>
							<p>Admin</p>
							<span class="badge"><?=$admin?></span>
							</a>
							<a class="quick-button metro green span2">
							<i class="icon-barcode"></i>
							<p>Superadmin</p>
							<span class="badge"><?=$superadmin?></span>
							</a>
							</div>
							<br><br>
						<form class="form-horizontal" method="post" action="<?=base_url()?>ticket">
						  <fieldset>
							
							<div class="control-group">
							  <label class="control-label" for="typeahead">Employee id</label>
							  <div class="controls">

								<input type="text" class="span6 typeahead" id="typeahead" value="<?=$_SESSION['username']?>" readonly disabled>
							  </div>
							</div>
							
							 <div class="control-group">
								<label class="control-label" for="selectError3">WorkStation</label>
								<div class="controls">
									<select id="selectError3" name="workstation" required="required">
									<?php 
										for ($i=1; $i <72 ; $i++) { 
											echo "<option value=".$i.">".$i."</option>";			
										}
										echo "</select>";
									?>
								</div>
							  </div>

							 
							 <div class="control-group">
								<label class="control-label" for="selectError3">Floor </label>
								<div class="controls">
								  <select id="selectError3" name="floor" >
									<option value="2nd Floor">2nd Floor</option>
									<option value="3rd Floor">3rd Floor</option>
									<option value="4th Floor">4th Floor</option>
								  </select>
								</div>
							  </div>

							<div class="control-group">
								<label class="control-label" for="selectError2">Issue Select</label>
								<div class="controls">
									<select data-placeholder="Types Of Issue" name="issue" id="selectError2" data-rel="chosen" required="required">
										<option value=""></option>
									<optgroup label="Desktop">
										  <option>Keyboard</option>
										  <option>Mouse</option>
										  <option>Headphone</option>
										  <option>Monitor</option>
										  <option>CPU</option>
										  <option>Power Suppply</option>
										  <option>Software</option>
										  <option>Hang</option>
										</optgroup>
										<optgroup label="Server & Network">
										  <option>Password Reset</option>
										  <option>Log On </option>
										  <option>Five9</option>
										  <option>8*8</option>
										  <option>AVOXI </option>
										  <option>Voice Issue </option>
										  <option>Website Issue </option>
										  <option>Website Open Request </option>
										  <option>Convox</option>
										  <option>Mail Password Reset</option>
										  <option>LMI Sesion Required</option>
										  <option>Call Recording Required</option>
										  <option>Shared Drive </option>
										  <option>Access Point</option>
										  <option>Convox</option>
										</optgroup>
										<optgroup label="Others">
										  <option>Others</option>
										</optgroup>
								  </select>
								</div>
							  </div>


							<div class="control-group hidden-phone">
							  <label class="control-label" for="textarea2">Comment Here</label>
							  <div class="controls">
								<textarea class="cleditor" id="textarea2" rows="3" name="comment"></textarea>
							  </div>
							</div>
							<div class="form-actions">
							  <input type="submit" class="btn btn-primary" name="submit" value="Generate">
							</div>
						  </fieldset>
						  <input type="hidden" name="username" value="<?=$_SESSION['username']?>">
						</form>   

					</div>
				</div><!--/span-->

			</div><!--/row-->		


	<?php endif;?>	

		<div class="clearfix"></div>

	</div>
	<div class="clearfix"><br></div>



	<!--/row-->



</div><!--/.fluid-container-->

<!-- end: Content -->
</div><!--/#content.span10-->
</div><!--/fluid-row-->

<div class="modal hide fade" id="myModal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">×</button>
		<h3>Settings</h3>
	</div>
	<div class="modal-body">
		<p>Here settings can be configured...</p>
	</div>
	<div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal">Close</a>
		<a href="#" class="btn btn-primary">Save changes</a>
	</div>
</div>

<div class="common-modal modal fade" id="common-Modal1" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-content">
		<ul class="list-inline item-details">
			<li><a href="http://themifycloud.com">Admin templates</a></li>
			<li><a href="http://themescloud.org">Bootstrap themes</a></li>
		</ul>
	</div>
</div>

<div class="clearfix"></div>

