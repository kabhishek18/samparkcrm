<!-- start: Content -->
<div id="content" class="span10">


	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="<?=base_url()?>">Home</a>
			<i class="icon-angle-right"></i>
		</li>
		<li><a href="#">Completed Issue</a></li>
	</ul>
	<div class="row-fluid sortable">
		<div class="box span12">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon white user"></i><span class="break"></span>Export CSV</h2>
				<div class="box-icon">
					<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content" >
				<center>					 <form method="post" action="<?php echo base_url(); ?>Ticket/action">
					     <input type="submit" name="export" class="btn btn-info" value="Export CSV" />
					 </form></center>
			</div>
		</div>
	</div>
	<div class="row-fluid sortable">
		<div class="box span12">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon white user"></i><span class="break"></span>Items List</h2>
				<div class="box-icon">
					<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped table-bordered bootstrap-datatable datatable">
					<thead>
					<tr>
						<th>#</th>
						<th>Issueid</th>
						<th>Alloted Date</th>
						<th>Resolving Date</th>
						<th>Username</th>
						<th>Issue</th>
						<th>Floor</th>
						<th>Workstation</th>
						<th>Status</th>
						<th>Done By</th>
						<th>Delete</th>
					</tr>
					</thead>
					<tbody>
					<?php $i=1; foreach ($item as $idata){?>
					<tr>
						<td><?=$i++?></td>
						<td>Softwill<?=$idata['id']?></td>

						<?php 
						$old_date = date($idata['created']);              // returns Saturday, January 30 10 02:06:34
						$old_date_timestamp = strtotime($old_date);
						$new_date = date('F , d y h:i:s', $old_date_timestamp);?>
						<td><?=$new_date?></td>
						<?php 
						$old_date1 = date($idata['modified']);              // returns Saturday, January 30 10 02:06:34
						$old_date_timestamp1 = strtotime($old_date1);
						$new_date1 = date('F , d y h:i:s', $old_date_timestamp1);?>
						<td><?=$new_date1?></td>


						<td><?=$idata['username']?></td>
						<td><?=$idata['issue']?></td>
						<td><?=$idata['floor']?></td>
						<td><?=$idata['workstation']?></td>
						<td><?=$idata['status']?></td>
						<td><?=$idata['doer']?></td>
						<form method="post" action="<?=base_url()?>travis/delete/<?=$idata['id']?>" onsubmit="return confirm('Are You Sure ?');"> 
							<input type="hidden" name="id" value="<?=$idata['id']?>">
						<td><button type="submit" class="btn btn-danger"  name="submit"><i class="halflings-icon white trash"></i></button></td>
						</form>
						
					</tr>
					<?php };?>
					</tbody>
				</table>
			</div>
		</div><!--/span-->

	</div><!--/row-->


</div><!--/.fluid-container-->

<!-- end: Content -->
</div><!--/#content.span10-->
</div><!--/fluid-row-->


<div class="clearfix"></div>
	