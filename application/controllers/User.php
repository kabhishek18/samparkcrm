<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('authen_model');
		$this->load->model('ticket_model');
		$this->load->model('user_model');
		if ($this->config->item('secure_site')) {
			force_ssl();
		}
	}	
	


	public function index()
	{
		
		$data['item'] = $this->user_model->UserViewer();
		$this->load->view('include/header');
		$this->load->view('include/nav');
		$this->load->view('user_view',$data);		
		$this->load->view('include/footer');
	}
	public function Add()
	{
		if($_SESSION['type']=='superadmin' || $_SESSION['type']=='developers'){

			$data['username']=$this->input->post("username");
			$data['name']=$this->input->post("name");
			$data['password']=$this->input->post("password");
			$data['type']=$this->input->post("type");
			$data['status']=$this->input->post("status");
			$this->user_model->UserAdd($data);
			echo "<script>alert('".$data['type']." Added')</script";
		redirect('user',refresh);

		}
		else{
			echo "<script>alert('Stop Trying Hacking! You Are NOt Hacker')</script>";
		}	
	}



	public function EditUser($id = null )
	{
		if($_SESSION['type']=='superadmin' || $_SESSION['type']=='developers')
		{
			$auth['username'] = $this->uri->segment(2,0);	
			$auth['id'] = $this->uri->segment(3,0);
			$data['item'] = $this->user_model->EditUserViewer($auth);
			$this->load->view('include/header');
			$this->load->view('include/nav');
			$this->load->view('useredit_view',$data);		
			$this->load->view('include/footer');
			
		}
		else{
			echo "<script>alert('Stop Trying Hacking! You Are NOt Hacker')</script>";
		}	


	}


	public function UpdateUser($id = null )
	{
		if($_SESSION['type']=='superadmin' || $_SESSION['type']=='developers')
		{
			$data['username']=$this->input->post("username");
			$data['password']=$this->input->post("password");
			$data['name']=$this->input->post("name");
			$data['type']=$this->input->post("type");
			$data['status']=$this->input->post("status");
			$data['id']=$this->input->post("id");
		   if($data['type']=='developers'){
		 		if( $_SESSION['type']=='developers')
					{
						$this->user_model->UpdateUserViewer($data);
		 			echo "<script>alert('Successfully Updated')</script>";
					redirect('user',refresh);
					}
					else{
				 	echo "<script>alert('Changes Not Possible')</script>";
				 	redirect('user',refresh);
					}

		 	}
		 	else
			{
			$this->user_model->UpdateUserViewer($data);
		 	echo "<script>alert('Successfully Updated')</script";
			redirect('user',refresh);
			
			}		
		}
		else{
			echo "<script>alert('Stop Trying Hacking! You Are NOt Hacker')</script>";
		}	


	}



	public function DeleteUser($id = null )
	{
		if($_SESSION['type']=='superadmin' || $_SESSION['type']=='developers')
		{

			$auth['username'] = $this->uri->segment(3,0);	
			$auth['id'] = $this->uri->segment(4,0);
			$data['item'] = $this->user_model->EditUserViewer($auth);
			if ($data['item'][0]['type']=='developers'  || $data['item'][0]['type']=='superadmin') {
				echo "<script>alert('Sorry Data is not Supposed to be Deleted')</script>";
				redirect('user',refresh);
			 }
			 else{
			 	$this->user_model->DeleteUser($auth);
				echo "<script>alert('Deleted Successfully')</script>";
				redirect('user',refresh);
			 } 


		}
		else
		{
			echo "<script>alert('Stop Trying Hacking! You Are NOt Hacker')</script>";
		}	


	}


	public function Change_password()
	{		
			$data['item']= 	$this->user_model->Change_passwordview($_SESSION['username']);
			$this->load->view('include/header');
			$this->load->view('include/nav');
			$this->load->view('change_password',$data);		
			$this->load->view('include/footer');
			
	}
	public function Change_passwordedit()
	{
		if($_SESSION['type']=='users' || $_SESSION['type']=='developers')
		{

			$auth['id']=$this->input->post("id");
			$auth['password']=$this->input->post("password");
		    $this->user_model->Change_passwordviewedit($auth);
		    echo "<script>alert('Password Has been Updated')</script>";
			$this->session->unset_userdata('uniqueid');	
			$this->session->unset_userdata('username');     
			$this->session->unset_userdata('name');      
			$this->session->unset_userdata('type');      

			$this->session->sess_destroy();
			redirect('',refresh);
		}
		else
		{
			echo "<script>alert('Stop Trying Hacking! You Are NOt Hacker')</script>";
			
		}	
	
			
	}




}
