<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('authen_model');
		$this->load->model('ticket_model');
		if ($this->config->item('secure_site')) {
			force_ssl();
		}
	}	
	


	public function index()
	{	
		if($this->session->userdata('uniqueid') == '')
		{
			$this->load->view('login',$data);
		}
		else{
		
			$data['user']=$this->authen_model->DashboardUserCount();
			$data['admin']=$this->authen_model->DashboardAdminCount();
			$data['superadmin']=$this->authen_model->DashboardSuperAdminCount();
			$data['developer']=$this->authen_model->DashboardDeveloperCount();

			$this->load->view('include/header');
			$this->load->view('include/nav');
			$this->load->view('dashboard',$data);
			$this->load->view('include/footer');
		}
	}	
}
