<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('authen_model');
		$this->load->helper('date');
		if ($this->config->item('secure_site')) {
			force_ssl();
		}
	}	
	
	public function index()
	{	
	if($this->session->userdata('uniqueid') == '')
		{
			$data = $_SERVER['HTTP_X_FORWARDED_FOR'];  
      			$data['ip'] = $_SERVER['REMOTE_ADDR']; 
  
			$this->load->view('login',$data);
		}
	else
	{	
		redirect('dashboard',refresh);
		exit();
	}
	}
	

	public function Authenthicate()
	{
		$auth['username']=$this->input->post("username");
		$auth['password']=$this->input->post("password");	
		date_default_timezone_set('Asia/Kolkata');
		$format = "%Y-%M-%d %H:%i";
		$auth['login'] = mdate($format);
		$data['auth']=$this->authen_model->Athentication($auth);
		$atimer =strtotime($auth['login']);
		$dtimer = strtotime($data['auth'][0]['login']);
		$sessiontime =round(abs($atimer - $dtimer) / 60,2);
		if ($sessiontime > 30) {
		  	// if($data['auth']==true)
		  	//  {
		  	//  	$this->session->set_userdata('uniqueid',$data['auth'][0]['id']);
		  	//  	$this->session->set_userdata('username',$data['auth'][0]['username']);
		  	//  	$this->session->set_userdata('name',$data['auth'][0]['name']);
		  	//  	$this->session->set_userdata('type',$data['auth'][0]['type']);
				
		  	//  	redirect('dashboard',refresh);
		  	//  }
		  	//  else{
		  	//  	echo "<script>alert('!Cautions.....Wrong Credentials')</script>";
		  	//  	redirect('',refresh);
		  	//  }
			}
		else{
			echo "<script>alert('Please wait for 30 min to session end')</script>";
		}	
	}

	public function PageNotFound($value='')
	{
		$this->load->view('404');
	}


	public function Logout()
	{
		if(session_destroy())
	{
		$this->session->unset_userdata('uniqueid');	
		$this->session->unset_userdata('username');     
		$this->session->unset_userdata('name');      
		$this->session->unset_userdata('type');      

		$this->session->sess_destroy();
		redirect('',refresh);
	}
	}

	public function test()
	{
		
	//	echo $update['id'] = generateUUID();
		//$this->load->view('include/header');
		//$this->load->view('include/nav');
		//$this->load->view('template_part/form');
		//$this->load->view('include/footer');
		// echo "<select>";	
		// for ($i=1; $i <72 ; $i++) { 
		// 	echo "<option>".$i."</option>";			
		// }
		// echo "</select>";
		// $array =array(
		// 	'AsusZen5',
		// 	'AsusZenselfie',
		// 	'AsusZenmax',
		// 	'AsusZen3max',
		// 	'AsusZenmaxpro',
		// 	'AsusZenmaxprom1',
		// 	'AsusZen3',
		// 	'AsusZenmaxprom2'



		// );
		// echo "<table>";
		// echo "<tr>";
		// foreach ($array as $item){
		// 	echo "<td>";
		// 	echo $item;
		// 	echo "</td>";
		// }
		// echo "</tr>";
		// for($i=1;$i<=1000;$i++){
		// 	echo "<tr>";
		// 	for($j=0;$j<=count($array)-1;$j++) {
		// 		echo "<td>". $array[$j]. "-" . $i . "</td>";
		// 	}
		// 	echo "</tr>";


		// }
		// echo "</table>";
	}

}
