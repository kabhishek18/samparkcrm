<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ticket extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('authen_model');
		$this->load->model('ticket_model');
		if ($this->config->item('secure_site')) {
			force_ssl();
		}
	}	
	


	public function index()
	{	
		
		$data['username']=$this->input->post("username");
		$data['comment']=$this->input->post("comment");
		$data['issue']=$this->input->post("issue");
		$data['floor']=$this->input->post("floor");
		$data['created']=date('Y-m-d,H:i:s');
		$data['workstation']=$this->input->post("workstation");
		$this->ticket_model->TicketGenerater($data);
		echo "<script>alert('Your Ticket Has Been Generated! Please wait until it get resolved')</script>";
		redirect('dashboard',refresh);
	}


	public function Ticket_pending()
	{	
		$data['item'] = $this->ticket_model->PendingTicketView();
		$this->load->view('include/header');
		$this->load->view('include/nav');
		$this->load->view('pending_view',$data);		
		$this->load->view('include/footer');
	}


	public function Ticket_done()
	{	
		$data['item'] = $this->ticket_model->DoneTicketView();
		$this->load->view('include/header');
		$this->load->view('include/nav');
		$this->load->view('done_view',$data);		
		$this->load->view('include/footer');
	}

	public function Ticket_delete()
	{	
		$data['id']=$this->input->post("id");
		$this->ticket_model->TicketDelete($data);
		 redirect('compelted',refresh);
	}


	public function Resolver()
	{
		 if($_SESSION['type']=='admin' || $_SESSION['type']=='superadmin' || $_SESSION['type']=='developers'){
		 $data['doer']=$this->input->post("doer");
		 $data['id']=$this->input->post("id");
		 $data['rescomment']=$this->input->post("rescomment");
		 $data['status']=$this->input->post("status");
		 $this->ticket_model->TicketResolver($data);
		 redirect('pending_ticket',refresh);
		}
		else{
			echo "<script>alert('Stop Trying Hacking! You Are NOt Hacker')</script>";
		}	 
	}


	// Testing Area Begins
	public function action()
	 {

	 	//$this->load->dbutil();
		//$query = $this->db->query("SELECT * FROM issue");
		//echo $this->dbutil->csv_from_result($query);
	
	 	 $metaData[] = array("id","username","issue","comment","floor","workstation","doer","created","modified","status","rescomment",);
       
        $customerInfo = $this->ticket_model->ExportCSV(); 
        foreach($customerInfo as $key=>$element) {
            $storData[] = array(
                'id' => $element['id'],
                'username' => $element['username'],
                'issue' => $element['issue'],
                'comment' => $element['comment'],
                'floor' => $element['floor'],
                'workstation' => $element['workstation'],
                'doer' => $element['doer'],
                'created' => $element['created'],
                'modified' => $element['modified'],
                'status' => $element['status'],
                'rescomment' => $element['rescomment'],
            );
        }
        $data = array_merge($metaData,$storData);
        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"Issue".".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        $handle = fopen('php://output', 'w');
        foreach ($data as $data) {
            fputcsv($handle, $data);
        }
            fclose($handle);
        exit;
	 }



	 public function View_ticket()
	 {
	 	if($_SESSION['username']){
	 	$auth = $_SESSION['username'];
	 	$data['item'] = $this->ticket_model->UserView_ticket($auth);
		$this->load->view('include/header');
		$this->load->view('include/nav');
		$this->load->view('user_ticket_view',$data);		
		$this->load->view('include/footer');
	 	}
		else{
			echo "Error Produced! Please go back to login Menu";
		}

	 }




}
