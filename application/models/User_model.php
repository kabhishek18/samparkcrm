<?php
class User_model extends CI_Model
{
	public function UserViewer()
	{	
		$this->db->select('*');
		$this->db->from('users');
		$array = array('deleted' => '0');
		$this->db->where($array);
		$query = $this->db->get();
		if($query->num_rows() !=0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}

	public function UserAdd($auth)
	{	
		$data = array(
			'username' => $auth['username'],
			'name' => $auth['name'],
			'password'=>$auth['password'],
			'type'=>$auth['type'],
			'status'=> $auth['status'],
			'deleted'=> '0',
  			  );

    	$query = $this->db->insert('users',$data);
		if($query)
		{
			return $query;
		}
		else
		{
			return false;
		}
	}


	public function EditUserViewer($auth)
	{	
		$this->db->select('*');
		$this->db->from('users');
		$array = array(
			'username' => $auth['username'],
			'id' => $auth['id'],
			'deleted' => '0');
		$this->db->where($array);
		$query = $this->db->get();
		if($query->num_rows() !=0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}

	public function UpdateUserViewer($auth)
	{	
		$data = [
            'username' => $auth['username'],
			'password'=>$auth['password'],
			'type'=>$auth['type'],
			'status'=> $auth['status'],
        ];
        $this->db->where('id', $auth['id']);
        $query = $this->db->update('users', $data);
		if($query)
		{
			return $query;
		}
		else
		{
			return false;
		}
	}



	public function DeleteUser($auth)
	{	
		$data = [
			'deleted'=> '1',
        ];
        $this->db->where('id', $auth['id']);
        $this->db->where('username', $auth['username']);
        $query = $this->db->update('users', $data);
		if($query)
		{
			return $query;
		}
		else
		{
			return false;
		}
	}

	public function Change_passwordview($auth)
	{
		$this->db->select('password');
		$this->db->from('users');
		$array = array('username'=>$auth,	'deleted' => '0');
		$this->db->where($array);
		$query = $this->db->get();
		if($query->num_rows() !=0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}

	public function Change_passwordviewedit($auth)
	{
		$data = [
			'password'=> $auth['password'],
        ];
        $this->db->where('id', $auth['id']);
        $query = $this->db->update('users', $data);
		if($query)
		{
			return $query;
		}
		else
		{
			return false;
		}
	}

}
