<?php
class Authen_model extends CI_Model
{
	
	public function Athentication($auth)
	{	
		$this->db->select('*');
		$this->db->from('users');
		$array = array('username' => $auth['username'],'password' => $auth['password'],'deleted' =>'0');
		$this->db->where($array);
		$query = $this->db->get();
		if($query->num_rows() !=0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}

	public function DashboardUserCount()
	 {
	 	$this->db->select('*');
		$this->db->from('users');
		$array = array('type' => 'users' ,'deleted' =>'0');
		$this->db->where($array);
		$query = $this->db->get();
		if($query->num_rows() !=0)
		{
			return $query->num_rows();
		}
		else
		{
			return false;
		}
	 } 
	 public function DashboardAdminCount()
	 {
	 	$this->db->select('*');
		$this->db->from('users');
		$array = array('type' => 'admin','deleted' =>'0' );
		$this->db->where($array);
		$query = $this->db->get();
		if($query->num_rows() !=0)
		{
			return $query->num_rows();
		}
		else
		{
			return false;
		}
	 } 
	 public function DashboardSuperAdminCount()
	 {
	 	$this->db->select('*');
		$this->db->from('users');
		$array = array('type' => 'superadmin' ,'deleted' =>'0');
		$this->db->where($array);
		$query = $this->db->get();
		if($query->num_rows() !=0)
		{
			return $query->num_rows();
		}
		else
		{
			return false;
		}
	 } 
	 public function DashboardDeveloperCount()
	 {
	 	$this->db->select('*');
		$this->db->from('users');
		$array = array('type' => 'developer','deleted' =>'0' );
		$this->db->where($array);
		$query = $this->db->get();
		if($query->num_rows() !=0)
		{
			return $query->num_rows();
		}
		else
		{
			return false;
		}
	 } 

}
