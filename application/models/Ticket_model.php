<?php
class Ticket_model extends CI_Model
{
	
	public function TicketGenerater($auth)
	{	
		$data = array(
		'username' => $auth['username'],
		'issue'  =>  $auth['issue'],
		'comment'  =>  $auth['comment'],
		'floor'  =>  $auth['floor'],
		'created' => $auth['created'],
		'workstation'  =>  $auth['workstation'],
		'status'	=> 'pending',
		'deleted'	=> '0',
		);

		$query = $this->db->insert('issue', $data);;	
		if($query)
		{
			return $query;
		}
		else
		{
			return false;
		}
	}

	public function PendingTicketView()
	{	
		$this->db->select('*');
		$this->db->from('issue');
		$array = array('status !=' => 'done','deleted'=>'0');
		$this->db->where($array);
		$query = $this->db->get();
		if($query->num_rows() !=0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}


	public function DoneTicketView()
	{	
		$this->db->select('*');
		$this->db->from('issue');
		$array = array('status' => 'done','deleted'=>'0');
		$this->db->where($array);
		$this->db->order_by("modified", "desc");
		$query = $this->db->get();
		if($query->num_rows() !=0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}

	public function ExportCSV()
	{	
		$this->db->select('*');
		$this->db->from('issue');
		$array = array('status' => 'done','deleted'=>'0');
		$this->db->where($array);
		$this->db->order_by("modified", "desc");
		$query = $this->db->get();
		if($query->num_rows() !=0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}

	public function TicketDelete($auth)
	{	
		$data = array(
	'deleted'=> '1'
	);
	$this->db->where('id', $auth['id']);
	$this->db->update('issue',$data);
	
	}


	public function TicketResolver($auth)
	{	
		$data = array(
	'doer' => $auth['doer'],
	'status'=> $auth['status'],
	'rescomment'=> $auth['rescomment']
	);
		var_dump($data);
	$this->db->where('id', $auth['id']);
	$this->db->update('issue',$data);
	
	}


	public function UserView_ticket($auth)
	{	
		$this->db->select('*');
		$this->db->from('issue');
		$array = array('username' => $auth,'deleted'=>'0');
		$this->db->where($array);
		$query = $this->db->get();
		if($query->num_rows() !=0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}


}
